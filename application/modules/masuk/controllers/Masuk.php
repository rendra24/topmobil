<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masuk extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	
		
		$this->load->view('masuk');
		

	}

	public function dologin()
	{
		if(!empty($this->input->post()))
		{
			$data = $this->input->post();
			$pass = md5(sha1($data['password']));
			$get = $this->db->get_where('users',array('username' => $data['username'] , 'password' => $pass));

			if($hasil = $get->row_array())
			{
				$sess = array('userid' => $hasil['user_id'], 'username' => $hasil['username'], 'level' => $hasil['level'], 'cabang' => $hasil['cabang']);
				$this->session->set_userdata($sess);

				$up['last_login'] = date('Y-m-d H:i:s');
				if($this->db->update("users",$up,array('user_id' => $hasil['user_id'])))
				{
					redirect(base_url().'admin','refresh');
				}
			}else{
				echo "tidak ada";
			}

		}else{
			redirect(base_url().'masuk','refresh');
		}
	}

	public function keluar()
	{

		$this->session->sess_destroy();
		redirect(base_url().'masuk','refresh');
	}

	public function rend()
	{
		exit;
		echo md5(sha1('123'));
	}

}
