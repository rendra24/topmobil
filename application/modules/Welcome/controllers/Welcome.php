<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		
		$isMobileSafari = preg_match('/(iPod|iPhone|iPad)/', $_SERVER['HTTP_USER_AGENT']);

		$get = $this->db->get_where('conter',array('ip' =>  $_SERVER['REMOTE_ADDR']));
		
		if($get->num_rows() == 0)
		{
			$browser = $_SERVER['HTTP_USER_AGENT'];
			$chrome = '/Chrome/';
			$firefox = '/Firefox/';
			$ie = '/MSIE/';
			$ie = '/MSIE/';
			if (preg_match($chrome, $browser))
			{
				$brow = "Chrome/Opera";
			}
			else if (preg_match($firefox, $browser))
			{
				$brow = "Firefox";
			}
			else if (preg_match($ie, $browser))
			{
				$brow = "IE";
			}else if($isMobileSafari == 1){
				$brow = "Safari";
			}else{
				$brow = "";
			}


			$add['ip'] = $_SERVER['REMOTE_ADDR'];
			$add['browser'] = $brow;
			$add['tgl'] = date('Y-m-d');
			$this->db->insert('conter',$add);
		}
		

		$service = $this->db->get_where("article",array('flag' => 1 ))->result_array();
		$data = array(
			'aktif' => 'home',
			'judul' => 'HOME',
			'service' => $service,
		);

		$this->load->view('header',$data);
		$this->load->view('home',$data);
		$this->load->view('footer');

	}
	public function about()
	{
		$this->db->order_by('id_article', 'desc');
		$this->db->where('flag','1');
		$article =  $this->db->get('article')->result_array();
		
		$data = array(
			'aktif' => 'about',
			'judul' => 'ABOUT US',
			'service' => $article,
		);
		$this->load->view('header',$data);
		$this->load->view('about',$data);
		$this->load->view('footer');

	}

	public function register()
	{
		
		$cabang = $this->db->get("cabang")->result_array();
		$data = array(
			'aktif' => 'register',
			'judul' => 'REGISTER',
			'cabang' => $cabang,
		);
		$this->load->view('header',$data);
		$this->load->view('register');
		$this->load->view('footer');

	}

	public function gallery()
	{
		
		$service = $this->db->get_where("gallery")->result_array();
		$data = array(
			'aktif' => 'gallery',
			'judul' => 'GALLERY',
			'gallery' => $service,
		);
		$this->load->view('header',$data);
		$this->load->view('gallery');
		$this->load->view('footer');

	}
	public function contact()
	{
		
		
		$data = array(
			'aktif' => 'contact',
			'judul' => 'CONTACT US',
		);
		$this->load->view('header',$data);
		$this->load->view('contac');
		$this->load->view('footer');

	}

	public function add_contact()
	{
		$data = $this->input->post();
		$add['nama'] = $data['nama'];
		$add['email'] = $data['email'];
		$add['subject'] = $data['subject'];
		$add['pesan'] = $data['pesan'];
		$add['tgl']= date('Y-m-d');

		if($this->db->insert('contact',$add))
		{
			echo 1;
		}else{
			echo 0;
		}
	}
	public function add_service()
	{

		$data = $this->input->post();
		$add['nama_user'] = $data['nama'];
		$add['email'] = $data['email'];
		$add['no_kendaraan'] = $data['no_kendaraan'];
		$add['nama_kendaraan'] = $data['nama_kendaraan'];
		$add['keluhan'] = $data['keluhan'];
		$add['cabang'] = $data['cabang'];
		$add['tlp'] = $data['tlp'];
		$add['tgl']= date('Y-m-d H:i:s');

		if($this->db->insert('user_service',$add))
		{
			echo 1;
		}else{
			echo 0;
		}
	}
}
