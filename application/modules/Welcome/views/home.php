<style type="text/css">
	.testimonial-info p {
    
    margin: 1em 0 0 0;

}
</style>
<!-- banner -->
	<div class="banner jarallax">
		<div class="banner-dot">
			<!-- <div class="header-top">
				<div class="container">
					<div class="header-top-left">
						<p><i class="fa fa-home" aria-hidden="true"></i> 1st Street , mexico city</p>
					</div>
					<div class="header-top-right">
						<p><i class="fa fa-phone" aria-hidden="true"></i> +1 234 567 8901</p>
					</div>
				</div>
			</div> -->
			<div class="header">
				<div class="container">
					<div class="header-left">
						<div>
							<h1>
								<a href="index.html">
									<img src="<?php echo base_url().'assets/images/logo-topmobil.png' ?>" style="height:150px;">
								</a>
							</h1>
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
			<div class="">
				<div class="container">
					<div class="slider">
						<div class="callbacks_container">
							<ul class="rslides callbacks callbacks1" id="slider4">
								<li>
									<div class="agileits-banner-info">
										<h3>Top Mobil <span>Body repaire & service</span></h3>
										<div class="w3-button">
											<a href="single.html">More</a>
										</div>
									</div>
								</li>
							
							</ul>
						</div>

					</div>
				</div>
			</div>
			<div class="header-right">
				<div class="container">
						<div class="top-nav">
							<nav class="navbar navbar-default">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
								<!--navbar-header-->
								<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
									<ul class="nav navbar-nav navbar-right">
										<li><a href="<?php echo base_url(); ?>" class="active">Home</a></li>
										<li><a href="<?php echo base_url().'welcome/about'; ?>" <?php if($aktif == 'about') { echo "class='active'"; }?> >About</a></li>
										<li><a href="<?php echo base_url().'welcome/gallery'; ?>" <?php if($aktif == 'gallery') { echo "class='active'"; }?> >Gallery</a></li>	
										<li><a href="<?php echo base_url().'blog'; ?>" <?php if($aktif == 'blog') { echo "class='active'"; }?> >Blog</a></li>
										<li><a href="<?php echo base_url().'welcome/contact'; ?>" <?php if($aktif == 'contact') { echo "class='active'"; }?> >Contact Us</a></li>
										<li><a href="<?php echo base_url().'welcome/register'; ?>" <?php if($aktif == 'contact') { echo "class='active'"; }?> >Register</a></li>
									</ul>	
									<div class="clearfix"> </div>	
								</div>
							</nav>
						</div>
						<div class="clearfix"> </div>
					</div>
			</div>
			<div class="banner-social">
				<div class="container">
					<div class="agileinfo-social-grids">
						<ul>
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-rss"></i></a></li>
							<li><a href="#"><i class="fa fa-vk"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //banner -->
	<!-- welcome -->
	<div class="welcome">
		<div class="container">
			<div class="w3ls-welcome-grids">
				
				
				<div class="col-md-12 w3l-welcome-right" style="text-align: center;">
					<h3>TOP MOBIL</h3>
					<h5>BODY REPAIRE & SERVICE</h5>
					<p>kami melayani jasa service mobil, dengan pelayanan terbaik, dan kami mampu bersaing dengan bisnis lainnya di sekitar. Kami buka beberapa tempat seperti di Malang, Jember, Kediri, Mojokerto, Probolinggo. DI tempat kami menyedikan banyak jasa untuk perbaikan mobil anda. </span></p>
					<div class="w3l-button">
						<a href="<?php echo base_url().'welcome/about' ?>">More</a>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- //welcome -->
	<!-- services -->
	<div class="services">
		<div class="col-md-5 w3ls-services-left">
			
		</div>
		<div class="col-md-7 w3ls-services-right">
			<h3>Our Services</h3>
			<div class="w3ls-services-icons">
				<div class="agileits-icon-grid">
					<div class="icon-left hvr-radial-out">
						<i class="fa fa-cog" aria-hidden="true"></i>
					</div>
					<div class="icon-right">
						<h5>Suspendisse consectetur dapibus volutpat</h5>
						<p>Phasellus dapibus felis elit, sed accumsan arcu gravida vitae. Nullam aliquam erat at lectus ullamcorper, nec interdum neque hendrerit.</p>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="agileits-icon-grid">
					<div class="icon-left hvr-radial-out">
						<i class="fa fa-heart" aria-hidden="true"></i>
					</div>
					<div class="icon-right">
						<h5>Lorem ipsum dolor sit amet, consectetur</h5>
						<p>Phasellus dapibus felis elit, sed accumsan arcu gravida vitae. Nullam aliquam erat at lectus ullamcorper, nec interdum neque hendrerit.</p>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="agileits-icon-grid">
					<div class="icon-left hvr-radial-out">
						<i class="fa fa-paper-plane" aria-hidden="true"></i>
					</div>
					<div class="icon-right">
						<h5>Suspendisse consectetur dapibus volutpat.</h5>
						<p>Phasellus dapibus felis elit, sed accumsan arcu gravida vitae. Nullam aliquam erat at lectus ullamcorper, nec interdum neque hendrerit.</p>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
		<div class="clearfix"> </div>
	</div>
	<!-- //services -->
	<!-- work -->
	<div class="work">
		<div class="container">
			<div class="agileits-heading">
				<h3>Latest work</h3>
			</div>
		</div>
		<div class="work-grids">
			<div id="owl-demo" class="owl-carousel owl-theme">
					<?php foreach ($service as $row) {
					?>
					<div class="item">
						<div class="work-grid-info">
							<img src="<?php echo base_url().'img_blog/'.$row['image']; ?>" alt="" />
							<div class="work-grid-caption"> 
								<i class="fa fa-wrench" aria-hidden="true"></i>
								<h4><?php echo $row['title']; ?></h4>
								<!-- <p>Nunc tincidunt</p> -->
							</div>
						</div>	
					</div>
					<?php } ?>
					
					
			</div>
		</div>
	</div>
	<!-- //work -->
	<!-- testimonial -->
	<div class="testimonial jarallax">
		<div class="testimonial-dot">
			<div class="container">
				<div class="agileits-heading testimonial-heading">
					<h3>Testimonial</h3>
				</div>
				<div class="wthree-testimonial-grid">
					<div class="slider">
						<div class="callbacks_container">
							<ul class="rslides" id="slider3">
								<li>
									<div class="w3-agile-testimonial">
										<div class="testimonial-info">
											<h4>Ganti Oli Mesin</h4>
											<p>Mobil saya avansa , saya suda lama berlangganan di TOP MOBIL cabang probolinggo . karena pelayananya yang cepat dan tempat yang stategis gampang di jangkau . Saya senang service mobil saya di TOP MOBIL . Terima kasih </p>
										</div>
										
										<div class="clearfix"> </div>
									</div>
								</li>
								<li>
									<div class="w3-agile-testimonial">
										<div class="testimonial-info"><br>
											<h4>Ganti EPS & Shockbreaker</h4>
											<p>Mobil saya honda freed glodakan, saya bawa ke deler honda estimasinya habis 23.000.000 karena harus ngganti EPS, ondersteel, Shockbreaker depan. Akhirnya saya bawa ke bengkel ini, habisnya hanya 9.500.000 . Saya berterima kasih banyak mobil saya normal kembali. Sukses terus buat bengkel TOP MOBIL.</p>
										</div>
										
										<div class="clearfix"> </div>
									</div>
								</li>
								<li>
									<div class="w3-agile-testimonial">
										<div class="testimonial-info"><br>
											<h4>Servis Air Radiator & Mesin mobil</h4>
											<p><?php echo strtolower("MOBIL SAYA NISSAN EXTRAIL BARU 2011. BERBUNYI GLODAKAN DAN LARI LARI, MENAKUTKAN, DAN AIR RADIATOR NAMBAH TERUS UDARA. SAYA BAWA KE DEALER NISSAN MENCAKUP BIAYA ESTIMASINYA 29.000.000 LEBIH. AKHIRNYA SAYA BAWA KE BENGKEL TOP MOBIL. SEHARI SUDAH SELESAI, HABIS 14.500.000 BERES SEMUA. PERMASALAHAN PADA MOBIL SAYA SUDAH BERES SEMUA. TERIMA KASIH BENGKEL TOP MOBIL, SEMOGA MENJADI LANGGANAN SAYA ALSO YA LAIN. BENGKEL YA LEBIH MURAH DEALER DARI RESMI. SEMOGA RAMAI TERUS DAN SUKSES. CIAMIK SORO SEHARI SELESAI."); ?></p>
										</div>
										
										<div class="clearfix"> </div>
									</div>
								</li>
							</ul>
						</div>
						<script>
							// You can also use "$(window).load(function() {"
							// $(function () {
							// 	// Slideshow 4
							// 	$("#slider3").responsiveSlides({
							// 		auto: true,
							// 		pager:false,
							// 		nav:false,
							// 		speed: 500,
							// 		namespace: "callbacks",
							// 		before: function () {
							// 			$('.events').append("<li>before event fired.</li>");
							// 		},
							// 		after: function () {
							// 			$('.events').append("<li>after event fired.</li>");
							// 		}
							// 	});
									
							// });
						</script>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //testimonial -->
	<!-- subscribe -->
	<div class="wthree-subscribe">
		<div class="container">
			<div class="agileits-heading">
				<h3>Newsletter</h3>
			</div>
			<div class="w3-agileits-subscribe-form">
				<form action="#" method="post">
					<input type="text" placeholder="Subscribe" name="Subscribe" required="">
					<button class="btn1">Subscribe</button>
				</form>
			</div>
		</div>
	</div>
	<!-- //subscribe -->
	