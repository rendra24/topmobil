<!-- footer -->
	<div class="w3-agile-footer">
		<div class="container">
			<div class="footer-grids">
				<div class="col-md-3 footer-grid">
					<div class="footer-grid-heading">
						<h4>Address</h4>
					</div>
					<?php 
						$i=1;
						$get = $this->db->get('cabang')->result_array();
						foreach ($get as $row) {
					?>

					<div class="footer-grid-info">
						<p><?php echo $row['cabang'] ?>
							<span><?php echo $row['alamat'] ?>.</span>
						</p>
						<p class="phone">Phone : <?php echo $row['tlp'] ?>
							
						</p>
					</div>
					<?php
					if($i == 2){
						break;
					}
					$i++;
						}
					 ?>

					 
					
					
				</div>
				<div class="col-md-3 footer-grid">
					<div class="footer-grid-heading">
						<h4>Navigation</h4>
					</div>
					<div class="footer-grid-info">
						<ul>
							<li><a href="<?php echo base_url();?>">Home</a></li>
							<li><a href="<?php echo base_url().'welcome/about';?>">About</a></li>
							<li><a href="<?php echo base_url().'gallery';?>">Gallery</a></li>
							<li><a href="<?php echo base_url().'blog';?>">Blog</a></li>
							<li><a href="<?php echo base_url().'welcome/contact';?>">Contact</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-3 footer-grid">
					<div class="footer-grid-heading">
						<h4>Newsletter</h4>
					</div>
					<div class="agile-footer-grid">
						<ul class="w3agile_footer_grid_list">
							<li>Ut aut reiciendis voluptatibus maiores <a href="#">http://example.com</a> alias, ut aut reiciendis.
								<span><i class="fa fa-twitter" aria-hidden="true"></i> 02 days ago</span></li>
							<li>Itaque earum rerum hic tenetur a sapiente delectus <a href="#">http://example.com</a><span><i class="fa fa-twitter" aria-hidden="true"></i> 03 days ago</span></li>
						</ul>
					</div>
				</div>
				<div class="col-md-3 footer-grid">
					<div class="footer-grid-heading">
						<h4>Follow</h4>
					</div>
					<div class="social">
						<ul>
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-rss"></i></a></li>
							<li><a href="#"><i class="fa fa-vk"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="agileits-w3layouts-copyright">
				<p>© 2018 TopMobil . All Rights Reserved  </p>
			</div>
		</div>
	</div>
	
<!-- //footer -->
	<script src="<?php echo base_url().'assets/js/jarallax.js'?>"></script>
	<script src="<?php echo base_url().'assets/js/SmoothScroll.min.js'?>"></script>
	<script type="text/javascript">
		/* init Jarallax */
		$('.jarallax').jarallax({
			speed: 0.5,
			imgWidth: 1366,
			imgHeight: 768
		})
	</script>

	<script src="<?php echo base_url().'assets/js/responsiveslides.min.js'?>"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets/js/move-top.js'?>"></script>
	<script type="text/javascript" src="<?php echo base_url().'assets/js/easing.js'?>"></script>
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
<script src="<?php echo base_url().'assets/js/owl.carousel.js'?>"></script>  
</body>	
</html>