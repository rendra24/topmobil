<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
<title>Carmotive an Industrial Category Bootstrap responsive Website Template | Home :: w3layouts</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords" content="Carmotive Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- bootstrap-css -->
<link href="<?php echo base_url().'assets/css/bootstrap.css' ?>" rel="stylesheet" type="text/css" media="all" />
<!--// bootstrap-css -->
<!-- css -->
<link rel="stylesheet" href="<?php echo base_url().'assets/css/style.css' ?>" type="text/css" media="all" />
<!--// css -->
<link rel="stylesheet" href="<?php echo base_url().'assets/css/owl.carousel.css'?>" type="text/css" media="all">
<link href="<?php echo base_url().'assets/css/owl.theme.css'?>" rel="stylesheet">
<!-- font-awesome icons -->
<link href="<?php echo base_url().'assets/css/font-awesome.css'?>" rel="stylesheet"> 
<!-- //font-awesome icons -->
<!-- font -->
<link href="//fonts.googleapis.com/css?family=Cinzel:400,700,900" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>
<!-- //font -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script> 
<script>
$(document).ready(function() { 
	$("#owl-demo").owlCarousel({
 
		autoPlay: 3000, //Set AutoPlay to 3 seconds
		autoPlay:true,
		items : 4,
		itemsDesktop : [640,5],
		itemsDesktopSmall : [414,4]
 
	});
	
}); 
</script>
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<![endif]-->
</head>
<body>
	<?php 	if($aktif != 'home'){
	?>


	
		<!-- banner -->
	<div class="banner jarallax">
		<div class="banner-dot">
			<!-- <div class="header-top">
				<div class="container">
					<div class="header-top-left">
						<p><i class="fa fa-home" aria-hidden="true"></i> 1st Street , mexico city</p>
					</div>
					<div class="header-top-right">
						<p><i class="fa fa-phone" aria-hidden="true"></i> +1 234 567 8901</p>
					</div>
				</div>
			</div> -->
			<div class="header">
				<div class="container">
					<div class="header-left">

						<div class="agileits-banner-info" style="margin-top: 30px;">
										<h3>Top Mobil <span>Body repaire &amp; service</span></h3>
										
									</div>


					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
			
			<div class="header-right about-top">
				<div class="container">
						<div class="top-nav">
							<nav class="navbar navbar-default">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
								<!--navbar-header-->
								<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
									<ul class="nav navbar-nav navbar-right">
										<li><a href="<?php echo base_url(); ?>">Home</a></li>
										<li><a href="<?php echo base_url().'welcome/about'; ?>" <?php if($aktif == 'about') { echo "class='active'"; }?> >About</a></li>
										<li><a href="<?php echo base_url().'welcome/gallery'; ?>" <?php if($aktif == 'gallery') { echo "class='active'"; }?> >Gallery</a></li>	
										<li><a href="<?php echo base_url().'blog'; ?>" <?php if($aktif == 'blog') { echo "class='active'"; }?> >Blog</a></li>
										<li><a href="<?php echo base_url().'welcome/contact'; ?>" <?php if($aktif == 'contact') { echo "class='active'"; }?> >Contact Us</a></li>
										<li><a href="<?php echo base_url().'welcome/register'; ?>" <?php if($aktif == 'register') { echo "class='active'"; }?> >Register</a></li>
									</ul>	
									<div class="clearfix"> </div>	
								</div>
							</nav>
						</div>
						<div class="clearfix"> </div>
					</div>
			</div>
			<div class="wthree-heading">
				<h2><?php echo $judul ?></h2>
			</div>
		</div>
	</div>
	<?php } ?>
	<!-- //banner -->
	