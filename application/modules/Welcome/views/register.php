<style type="text/css">
	.custom_select
	{
		    margin-bottom: 1em;
    border: solid 1px #919191;
    border-radius: 2px;
       height: 2.8em;
    background: none;
    color: #919191;
	}
</style>
<!-- contact -->
	<div class="contact-top">
		<!-- container -->
		<div class="container">
			
			<div class="mail-grids">
				<div class="col-md-12 mail-grid-left">
					<h3>Pendaftaran Service</h3>				
				</div>
				<div class="col-md-12 contact-form">
					<form method="post" id="form-contact">
						<select class="form-control custom_select" name="cabang" id="cabang" placeholder="gaga">
							<option selected="">-- Pilih Cabang --</option>
								
							<?php foreach ($cabang as $row): ?>
								<option value="<?php echo $row['id_cabang'] ?>"><?php echo $row['cabang'] ?></option>
							<?php endforeach ?>
						</select>
						<input type="text" name="Name" id="nama" placeholder="Nama" required="">
						<input type="text" name="tlp" id="tlp" placeholder="Telephone/Hp" required="">
						<input type="email" name="Email" id="email" placeholder="Email" required="">
						<input type="text" name="no_kendaraan" id="no_kendaraan" placeholder="Nomor Kendaraan" required="">
						<input type="text" name="Name" id="nama_kendaraan" placeholder="Nama Kendaraan" required="">
						<textarea name="keluhan" id="keluhan" placeholder="Keluhan / kerusakan" required=""></textarea>
						<button type="button" id="kirim">Kirim</button>
					</form>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		<!-- //container -->
	</div>
	<!-- //contact -->
	<script type="text/javascript">
		$('#kirim').on('click', function(e) {
			var cabang = $('#cabang').val();
			var nama = $('#nama').val();
			var email = $('#email').val();
			var no_kendaraan = $('#no_kendaraan').val();
			var nama_kendaraan = $('#nama_kendaraan').val();
			var keluhan = $('#keluhan').val();
			var tlp = $('#tlp').val();

			$.ajax({
    	        type: "POST",
    	         url: "<?php echo base_url().'welcome/add_service'; ?>",
    	        data: {cabang:cabang, nama:nama, email:email, no_kendaraan:no_kendaraan, nama_kendaraan:nama_kendaraan, keluhan:keluhan, tlp:tlp},
    	        success: function(data){
    	       		alert(data);
    	        }	            
    	    });
		});
			  
		
	</script>