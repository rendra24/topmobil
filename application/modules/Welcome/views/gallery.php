<!-- gallery -->
	<div class="gallery">
		<div class="container">
			<div class="gallery-grids">
					<ul id="da-thumbs" class="da-thumbs">

						<?php foreach ($gallery as $row) {
						?>
						<li>
							<a href="images/w1.jpg" class=" mask b-link-stripe b-animate-go   swipebox"  title="">
								<img src="<?php echo base_url().'gallery/'.$row['image']?>" alt="" />
							</a>
						</li>
						
						<?php } ?>
					</ul>
					<div class="clearfix"> </div>
				
				<!--swipebox -->	
				<script src="js/jquery.swipebox.min.js"></script> 
				<script type="text/javascript">
					jQuery(function($) {
						$(".swipebox").swipebox();
					});
				</script>
				<!--//swipebox Ends -->
			</div>
		</div>
	</div>
	<!-- //gallery -->