<!-- contact -->
	<div class="contact-top">
		<!-- container -->
		<div class="container">
			<div class="map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d158858.18237072596!2d-0.10159865000003898!3d51.52864165000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1436514341845" allowfullscreen></iframe>
			</div>
			<div class="mail-grids">
				<div class="col-md-12 mail-grid-left">
					<h3>Address</h3>
				<div class="col-md-4">
					<h4>Malang</h4>
					<p>
						<span>JL A Yani 92-94</span>
						Telephone: Telp 0341 - 486799 (Hunting)
						<span>FAX: 0341 - 407123</span>
						
					</p>

					<h4>Kediri</h4>
					<p>
						<span>Jl Supersemar No 12</span>
						Telp 0354 - 4674799
						<span>Fax. 0354 - 671719</span>
						
					</p>
				</div>	
				<div class="col-md-4">
					<h4>Jember</h4>
					<p>
						<span>Jl Imam bonjol No 18</span>
						Telp 0331 - 339599
						<span>Fax. 0331 - 338599</span>
						
					</p>
					<h4>Probolinggo</h4>
					<p>
						<span>Jl Brantas No 248</span>
						Telp, 0335 - 425399
						<span>Fax. 0335 - 425299</span>
						
					</p>
				</div>
				<div class="col-md-4">
					<h4>Mojokerto</h4>
					<p>
						<span>Jl Empunala No 425</span>
						Telp 0321 - 5281899
						<span>Fax. 0321 - 5281699</span>
						
					</p>
				</div>
					
					
				</div>
				<div class="col-md-12 contact-form">
					<form method="post" id="form-contact">
						<input type="text" name="Name" id="nama" placeholder="Name" required="">
						<input type="email" name="Email" id="email" placeholder="Email" required="">
						<input type="text" name="Subject" id="subject" placeholder="Subject" required="">
						<textarea name="Message" id="pesan" placeholder="Message" required=""></textarea>
						<button type="button" id="kirim">Kirim</button>
					</form>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		<!-- //container -->
	</div>
	<!-- //contact -->
	<script type="text/javascript">
		$('#kirim').on('click', function(e) {
			var nama = $('#nama').val();
			var email = $('#email').val();
			var subject = $('#subject').val();
			var pesan = $('#pesan').val();
			$.ajax({
    	        type: "POST",
    	         url: "<?php echo base_url().'welcome/add_contact'; ?>",
    	        data: {nama:nama, email:email, subject:subject, pesan:pesan},
    	        success: function(data){
    	       		alert(data);
    	        }	            
    	    });
		});
			  
		
	</script>