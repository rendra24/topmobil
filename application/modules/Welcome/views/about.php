<!-- about -->
<div class="about">
	<div class="container">
		<div class="agileits-heading">
			<h3>Who we are</h3>
		</div>
		<div class="w3_about_grids">
			<div class="col-md-6 w3_about_grid_left">
				<h5>Services</h5>
				<div class="panel-group about_panel" id="accordion" role="tablist" aria-multiselectable="true">

					<?php
					$no=1;
					foreach ($service as $row) { ?>	
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingOne<?php echo $no; ?>">
							<h4 class="panel-title asd">
								<a class="pa_italic" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php echo $no; ?>" aria-expanded="true" aria-controls="collapseOne<?php echo $no; ?>">
									<span class="glyphicon glyphicon-plus" aria-hidden="true"></span><i class="glyphicon glyphicon-minus" aria-hidden="true"></i><?php echo $row['title']; ?>
								</a>
							</h4>
						</div>
						<div id="collapseOne<?php echo $no; ?>" class="panel-collapse collapse <?php if($no == 1){ echo "in"; }  ?>" role="tabpanel" aria-labelledby="headingOne<?php echo $no; ?>">
							<div class="panel-body panel_text">
								<?php echo substr($row['isi'], 0,50); ?>
							</div>
						</div>
					</div>
					<?php
					$no++; }  ?>
				</div>
			</div>
			<div class="col-md-6 w3_about_grid_right" style="text-align: center;margin-top: 50px;">
				<img src="<?php echo base_url().'assets/images/logo.jpeg';?>" alt="" style="    height: 250px;width: auto;"/>
				<h3>TOP MOBIL <br> BODY REPAIRE & SERVICE</h3>

			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!-- //about -->
<!-- team -->
	<!-- <div class="team jarallax">
		<div class="team-dot">
			<div class="container">
				<div class="agileits-heading team-heading">
					<h3>Our Team</h3>
				</div>
				<div class="team-grids">
					<div class="col-md-3 agileinfo-team-grid">
						<img src="<?php echo base_url().'assets/images/t1.jpg';?>" alt="">
						<div class="captn">
							<h4>Mary Jane</h4>
							<p>Vestibulum </p>
							<div class="w3l-social">
								<ul>
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-rss"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-3 agileinfo-team-grid">
						<img src="<?php echo base_url().'assets/images/t2.jpg';?>" alt="">
						<div class="captn">
							<h4>Peter Parker</h4>
							<p>Aliquam non</p>
							<div class="w3l-social">
								<ul>
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-rss"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-3 agileinfo-team-grid">
						<img src="<?php echo base_url().'assets/images/t3.jpg';?>" alt="">
						<div class="captn">
							<h4>Johan Botha</h4>
							<p>Nulla molestie</p>
							<div class="w3l-social">
								<ul>
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-rss"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-3 agileinfo-team-grid">
						<img src="<?php echo base_url().'assets/images/t4.jpg'?>" alt="">
						<div class="captn">
							<h4>Steven Wilson</h4>
							<p>Quisque vitae</p>
							<div class="w3l-social">
								<ul>
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-rss"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
	</div> -->
	<!-- //team -->
	<!-- about-top -->
	<div class="agileits-about-top">
		<div class="container">
			<div class="agileits-heading">
				<h3>What we are doing</h3>
			</div>
			<div class="agileinfo-top-grids">
				<div class="col-sm-4 wthree-top-grid">
					<img src="images/w1.jpg" alt="" />
					<h4>Curabitur non blandit justo</h4>
					<p>Pellentesque auctor euismod lectus a pretium. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur ut elit at est tempus volutpat.nascetur ridiculus mus. Curabitur ut elit at est tempus volutpat.</p>
				</div>
				<div class="col-sm-4 wthree-top-grid">
					<img src="images/w3.jpg" alt="" />
					<h4>Curabitur non blandit justo</h4>
					<p>Pellentesque auctor euismod lectus a pretium. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur ut elit at est tempus volutpat.nascetur ridiculus mus. Curabitur ut elit at est tempus volutpat.</p>
				</div>
				<div class="col-sm-4 wthree-top-grid">
					<img src="images/w7.jpg" alt="" />
					<h4>Curabitur non blandit justo</h4>
					<p>Pellentesque auctor euismod lectus a pretium. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur ut elit at est tempus volutpat.nascetur ridiculus mus. Curabitur ut elit at est tempus volutpat.</p>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- //about-top -->
	