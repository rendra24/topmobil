<script src="<?php echo base_url().'tinymce/tinymce.min.js' ?>"></script>
<script src="<?php echo base_url().'tinymce/jquery.tinymce.min.js' ?>"></script>
<?php echo form_open_multipart('admin/edit/'.$article['id_article']); ?>

<div class="row">
        <div class="col-md-6">
	
	<div>
	    <label>Meta Title : </label>
		<input type="text" name="meta_title" class="form-control form-sm" value="<?php echo ($this->input->post('meta_title') ? $this->input->post('meta_title') : $article['meta_title']); ?>" />
	</div>
	<div>
		<label>Meta Keyword : </label>
		<input type="text" name="meta_keyword"  class="form-control form-sm" value="<?php echo ($this->input->post('meta_keyword') ? $this->input->post('meta_keyword') : $article['meta_keyword']); ?>" />
	</div>
	<div>
		<label>Meta Desc : </label>
		<textarea name="meta_desc" class="form-control form-sm"><?php echo ($this->input->post('meta_desc') ? $this->input->post('meta_desc') : $article['meta_desc']); ?></textarea>
	</div>
    <div>
	     <br>
		<label>Image : </label>
		<input type="file" name="userfile" id="userfile" value="<?php echo $this->input->post('image'); ?>" />
		
		<div id="form-foto-box-image2" style="width: 180px; background: #FFF; height: 110px; border: 1px solid #e6e6e6; padding: 20px; text-align: center; padding-top: 45px; font-size: 14px; float: left;display:none;"></div>
	</div>
</div>
 <div class="col-md-6">
<div>
		<label>Title : </label>
		<input type="text" name="title" class="form-control form-sm" value="<?php echo ($this->input->post('title') ? $this->input->post('title') : $article['title']); ?>" />
	</div>
	<div>
		<label>Img Title : </label>
		<input type="text" name="img_title" class="form-control form-sm" value="<?php echo ($this->input->post('img_title') ? $this->input->post('img_title') : $article['img_title']); ?>" />
	</div>
	<div>
		<label>Img Alt : </label>
		<input type="text" name="img_alt" class="form-control form-sm" value="<?php echo ($this->input->post('img_alt') ? $this->input->post('img_alt') : $article['img_alt']); ?>" />
	</div>
	<div>
		<label>Img Capt : </label>
		<input type="text" name="img_capt" class="form-control form-sm" value="<?php echo ($this->input->post('img_capt') ? $this->input->post('img_capt') : $article['img_capt']); ?>" />
	</div>
	 
</div>	
<div class="col-md-12">
	<div>
		<label>Isi : </label>
		<textarea name="isi" class="form-control form-sm" id="myid"><?php echo ($this->input->post('isi') ? $this->input->post('isi') : $article['isi']); ?></textarea>
		<input type="hidden" name="flag" value="<?php echo ($this->input->post('flag') ? $this->input->post('flag') : $article['flag']); ?>">
	</div>
	</div>
	
	
	 <div class="col-md-12">
	     <br>
	    <center>
	<button type="submit" class="btn btn-primary col-md-3">Save</button>
	</center>
	</div>
	</div>
	
<?php echo form_close(); ?>

 <script>
    
    
    
       function getSize(input) {

      var fileInput =  document.getElementById(input);
      var userfile_size;
      try{
		        userfile_size=fileInput.files[0].size; // Size returned in bytes.
          }catch(e){
            var objFSO = new ActiveXObject("Scripting.FileSystemObject");
            var e = objFSO.getFile( fileInput.value);
            var fileSize = e.size;
            userfile_size=fileSize;    
          }
          return userfile_size;
        }
        
         function hasExtension(inputName, exts) {
      var fileName = $('input[name='+inputName+']').val().toLowerCase();
		//return fileName;
		return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
	}
	
	function readURL(input,obj) {

		if (input.files && input.files[0]) {

			var reader = new FileReader();

			reader.onload = function (e) {
				document.getElementById(obj).style.backgroundImage = 'url(' + e.target.result + ')';
				document.getElementById(obj).style.backgroundSize = 'cover';
			}

			reader.readAsDataURL(input.files[0]);
		}
	}
        $(document).ready(function() {
            tinyMCE.init({
                selector: "#myid",
                 height : "220",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste jbimages"
                    ],
                    // ===========================================
                    // PUT PLUGIN'S BUTTON on the toolbar
                    // ===========================================
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
                    // ===========================================
                    // SET RELATIVE_URLS to FALSE (This is required for images to display properly)
                    // ===========================================
                    relative_urls : false,
                    remove_script_host : false,
                    document_base_url: "http://www.karirpad.com/artikel/",
                    convert_urls : true
            });
            
             $("#userfile").change(function(){
          userphoto_size = getSize('userfile');
          if(hasExtension('userfile', ['.png', '.jpg', '.jpeg'])===false){ 
            document.getElementById("errpic").innerHTML = "Format foto harus JPG / PNG";
            document.getElementById("form-foto-box-image").style.backgroundImage = '';
             $('#no_file').show();
          
          }else if(userphoto_size > 20480000){
            document.getElementById("errpic").innerHTML = "Size file foto Anda tidak boleh lebih dari 20MB";
            document.getElementById("form-foto-box-image").style.backgroundImage = '';
             $('#no_file').show();
          
          }else{
            $('#form-foto-box-image').show();
            readURL(this,'form-foto-box-image');
             $('#no_file').hide();
            document.getElementById("errpic").innerHTML = "";
           
            error =0;
          }
        });
        });
        
        
      
      
    </script>