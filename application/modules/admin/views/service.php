
 
<!-- Basic Examples -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                  <?php echo $judul; ?>
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul>
            </div>

            <div class="body">
               
                    
               
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>nama</th>
                                 <th>Telephone/Hp</th>
                                <th>email</th>
                                <th>No Polisi</th>
                                <th>Nama Kendaraan</th>
                                <th>Keluhan/Kerusakan</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no=1;
                             foreach($service as $a){ ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $a['nama_user']; ?></td>
                                 <td><?php echo $a['tlp']; ?></td>
                                <td><?php echo $a['email']; ?></td>
                                <td><?php echo $a['no_kendaraan']; ?></td>
                                <td><?php echo $a['nama_kendaraan']; ?></td>
                                <td><?php echo $a['keluhan']; ?></td>
                               
                               
                                <td>
                                 
                                 <a href="<?php echo site_url('admin/form_cabang/'.$a['id_service']); ?>" class="btn btn-success btn-sm">Call</a>
                                 <a href="<?php echo site_url('admin/remove_cabang/'.$a['id_service']); ?>" class="btn btn-primary btn-sm">Done</a>
                             </td>
                         </tr>
                         <?php 
                         $no++;
                          } ?>
                     </tbody>
                 </table>
             </div>
         </div>
     </div>
 </div>
</div>
<!-- #END# Basic Examples -->   