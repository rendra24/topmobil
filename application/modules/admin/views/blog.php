
 
<!-- Basic Examples -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                   <?php if($flag == 0){ echo "ARTICLE";}else{echo "SERVICES";} ?>
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);">Action</a></li>
                            <li><a href="javascript:void(0);">Another action</a></li>
                            <li><a href="javascript:void(0);">Something else here</a></li>
                        </ul>
                    </li>
                </ul>
            </div>

            <div class="body">
               
                    <a href="<?php echo site_url('admin/add');?>/<?php if($flag == 0){ echo "0";}else{echo "1";} ?>" class="btn btn-primary">Add Artcile</a><br><br>
               
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Meta Title</th>
                                <th>Image</th>
                                <th>Img Title</th>
                                <th>Img Alt</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no=1;
                             foreach($article as $a){ ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo substr($a['title'], 0,20); ?></td>
                                <td><?php echo $a['meta_title']; ?></td>
                            
                                <td><?php echo $a['image']; ?></td>
                                <td><?php echo $a['img_title']; ?></td>
                                <td><?php echo $a['img_alt']; ?></td>
                               
                                <td>
                                 <a href="<?php echo site_url('admin/edit/'.$a['id_article']); ?>">View</a><br>
                                 <a href="<?php echo site_url('admin/edit/'.$a['id_article']); ?>">Edit</a><br>
                                 <a href="<?php echo site_url('admin/remove/'.$a['id_article']); ?>">Delete</a>
                             </td>
                         </tr>
                         <?php 
                         $no++;
                          } ?>
                     </tbody>
                 </table>
             </div>
         </div>
     </div>
 </div>
</div>
<!-- #END# Basic Examples -->   