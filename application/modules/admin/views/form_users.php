<style type="text/css">
	.btn:not(.btn-link):not(.btn-circle) span {
    position: relative;
    top: 2px;
    margin-left: 3px;
}
</style>
<?php 
if(isset($id)){ 
	echo form_open('admin/edit_users/'.$id);
}else { 
	echo form_open('admin/add_users'); 
} 
?>

<div class="row">
	<div class="col-md-12">


		<div class="col-md-6">
			<div>
				<label>Username:</label> 
				<input type="text" name="username" id="meta_title"  class="form-control form-sm" value="<?php if(isset($id)){ echo $users['username']; } ?>" />

			</div>
			<div>
				<br>
				<?php 
				if(isset($id)){
					echo "<label>New Password</label>";
					}else{
						echo "<label>Password</label>";
					}
				?>
				
				<input type="password" name="password"  class="form-control form-sm" />

			</div>
		</div>

		<div class="col-md-6">
			<div>

				<label>Cabang : </label>
				<select  name="cabang" class="form-control show-tick">
					<option>-- Pilih Cabang --</option>
					<?php foreach($cabang as $row) { ?>
<option value="<?php echo $row['id_cabang']; ?>" <?php if(isset($id)){ if($users['cabang'] == $row['id_cabang']){ echo "selected"; } } ?>><?php echo $row['cabang']; ?></option>
					<?php } ?>
				</select>

			</div>
			<div>
				<br>
				<label>Jabatan : </label>
				<select class="form-control show-tick" name="jabatan"> 
					<option>-- Pilih Jabatan --</option>
					<option value="1" <?php if(isset($id)){ if($users['level'] == 1){ echo "selected"; } } ?>>Super Admin</option>
					<option value="2" <?php if(isset($id)){ if($users['level'] == 2){ echo "selected"; } }?>>Admin</option>
				</select>

			</div>
		</div>	
		<div class="col-md-12" style="margin-bottom: 20px;padding-top: 30px;text-align: center;">
			<center>
				<button type="submit" class="btn btn-primary col-md-3">Save</button>
			</center>

		</div>
	</div>


	
	
</div>

<?php echo form_close(); ?>
