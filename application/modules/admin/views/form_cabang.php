
<?php 
if(isset($id)){ 
	echo form_open('admin/edit_cabang/'.$id);
}else { 
	echo form_open('admin/add_cabang'); 
} 
?>

<div class="row">
	<div class="col-md-12">


		<div class="col-md-6">
			<div>
				<label>Nama Cabang:</label> 
				<input type="text" name="cabang" id="meta_title"  class="form-control form-sm" value="<?php if(isset($id)){ echo $cabang['cabang']; } ?>" />

			</div>
			<div>
				<br>
				<label>Alamat Cabang</label>
				<input type="text" name="alamat"  value="<?php if(isset($id)){ echo $cabang['alamat']; } ?>"  class="form-control form-sm" />

			</div>
		</div>

		<div class="col-md-6">
			<div>

				<label>Tlp : </label>
				<input type="text" name="tlp" id="meta_desc" class="form-control form-sm" value="<?php if(isset($id)){ echo $cabang['tlp']; } ?>" ></textarea>

			</div>
			<div>
				<br>
				<label>Fax : </label>
				<input type="text" name="fax" id="meta_desc" class="form-control form-sm" value="<?php if(isset($id)){ echo $cabang['fax']; } ?>" ></textarea>

			</div>
		</div>	
		<div class="col-md-12" style="margin-bottom: 20px;padding-top: 30px;text-align: center;">
			<center>
				<button type="submit" class="btn btn-primary col-md-3">Save</button>
			</center>

		</div>
	</div>


	
	
</div>

<?php echo form_close(); ?>
