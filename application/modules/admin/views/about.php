

<!-- Basic Examples -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                  Gallery
              </h2>
              <ul class="header-dropdown m-r--5">
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="material-icons">more_vert</i>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="javascript:void(0);">Action</a></li>
                        <li><a href="javascript:void(0);">Another action</a></li>
                        <li><a href="javascript:void(0);">Something else here</a></li>
                    </ul>
                </li>
            </ul>
        </div>

        <div class="body">

            

            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nama Perusahaan</th>
                            <th>Logo</th>
                            <th>keterangan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no=1;
                        foreach($about as $a){ ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                             <td><?php echo $a['perusahaan']; ?></td>
                            <td style="text-align: center;"><img src="<?php echo base_url().'assets/images/'.$a['logo']; ?>" style="width: 150px;height: auto;"></td>
                            <td><?php echo $a['keterangan']; ?></td>
                            <td>
                              
                              
                               <a href="<?php echo site_url('admin/edit_about/'.$a['id']); ?>" class="btn btn-warning btn-sm">Edit</a>
                           </td>
                       </tr>
                       <?php 
                       $no++;
                   } ?>
               </tbody>
           </table>
       </div>
   </div>
</div>
</div>
</div>
<!-- #END# Basic Examples -->   