

<?php echo  form_open_multipart('admin/insert_gallery'); ?>

<script>
	function hitung() {
		var str =  $('#meta_title').val();
		var n = str.length;
		document.getElementById("demo").innerHTML = n+' characters. Most search engines use a maximum of 60 chars for the title.';
	}
	function hitung2() {
		var str =  $('#meta_desc').val();
		var n = str.length;
		document.getElementById("demo2").innerHTML = n+' characters. Most search engines use a maximum of 60 chars for the title.';
	}
</script>
<div class="row">


	<div class="col-md-12" style="margin-bottom: 20px;padding-top: 30px;">
		<div>
			<br>
			<label>Image : </label>
			<input type="file" name="userfile" id="userfile"  value="<?php echo $this->input->post('image'); ?>" />

			<div id="form-foto-box-image" style="width: 180px; background: #FFF; height: 110px; border: 1px solid #e6e6e6; padding: 20px; text-align: center; padding-top: 45px; font-size: 14px; float: left;display:none;"></div>
		</div>
		<br>
		<center>
			<button type="submit" class="btn btn-primary col-md-3">Save</button>
		</center>

	</div>
	
	<?php echo form_close(); ?>

	<script>



		function getSize(input) {

			var fileInput =  document.getElementById(input);
			var userfile_size;
			try{
		        userfile_size=fileInput.files[0].size; // Size returned in bytes.
		    }catch(e){
		    	var objFSO = new ActiveXObject("Scripting.FileSystemObject");
		    	var e = objFSO.getFile( fileInput.value);
		    	var fileSize = e.size;
		    	userfile_size=fileSize;    
		    }
		    return userfile_size;
		}

		function hasExtension(inputName, exts) {
			var fileName = $('input[name='+inputName+']').val().toLowerCase();
		//return fileName;
		return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
	}
	
	function readURL(input,obj) {

		if (input.files && input.files[0]) {

			var reader = new FileReader();

			reader.onload = function (e) {
				document.getElementById(obj).style.backgroundImage = 'url(' + e.target.result + ')';
				document.getElementById(obj).style.backgroundSize = 'cover';
			}

			reader.readAsDataURL(input.files[0]);
		}
	}
	$(function() {


		$("#userfile").change(function(){
			userphoto_size = getSize('userfile');
			if(hasExtension('userfile', ['.png', '.jpg', '.jpeg'])===false){ 
				alert("Format foto harus JPG / PNG");
				document.getElementById("form-foto-box-image").style.backgroundImage = '';
				$('#no_file').show();

			}else if(userphoto_size > 20480000){
				alert("Size file foto Anda tidak boleh lebih dari 20MB");
				document.getElementById("form-foto-box-image").style.backgroundImage = '';
				$('#no_file').show();

			}else{
				$('#form-foto-box-image').show();
				readURL(this,'form-foto-box-image');
				$('#no_file').hide();
				

				error =0;
			}
		});
	});




</script>