<?php

class Article_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
   
    function get_article($id_article)
    {
        return $this->db->get_where('article',array('id_article'=>$id_article))->row_array();
    }
        
   
    function get_all_article()
    {
        $this->db->order_by('id_article', 'desc');
        return $this->db->get('article')->result_array();
    }
        
   
    function add_article($params)
    {
        $this->db->insert('article',$params);
        return $this->db->insert_id();
    }
    
   
    function update_article($id_article,$params)
    {
        $this->db->where('id_article',$id_article);
        return $this->db->update('article',$params);
    }
    
   
    function delete_article($id_article)
    {
        return $this->db->delete('article',array('id_article'=>$id_article));
    }
}
