<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

  public function __construct()
  {
    parent::__construct();

    if(!isset($_SESSION['userid']))
    {
        redirect(base_url().'masuk','refresh');
    }
}


public function index()
{

  $data = array(
     'aktif' => 'blog',
     'judul' => 'BLOG',
 );

  $this->load->view('header',$data);
  $this->load->view('home');
  $this->load->view('footer');

}

public function blog($flag=0)
{
  $this->db->order_by('id_article', 'desc');
  $this->db->where('flag',$flag);
  $article =  $this->db->get('article')->result_array();
  $data = array(
     'aktif' => 'blog',
     'judul' => 'BLOG',
     'flag' => $flag,
     'article' => $article,
 );

  $this->load->view('header',$data);
  $this->load->view('blog',$data);
  $this->load->view('footer');
}

public function gallery()
{
  $this->db->order_by('id_gallery', 'desc');
  $gallery =  $this->db->get('gallery')->result_array();
  $data = array(
     'aktif' => 'gallery',
     'judul' => 'gallery',
     'gallery' => $gallery,
 );

  $this->load->view('header',$data);
  $this->load->view('gallery',$data);
  $this->load->view('footer');
}

public function about()
{

  $gallery =  $this->db->get('about')->result_array();
  $data = array(
     'aktif' => 'gallery',
     'judul' => 'gallery',
     'about' => $gallery,
 );

  $this->load->view('header',$data);
  $this->load->view('about',$data);
  $this->load->view('footer');
}

public function cabang()
{
    $this->db->order_by('id_cabang', 'desc');

    $cabang =  $this->db->get('cabang')->result_array();
    $data = array(
        'aktif' => 'cabang',
        'judul' => 'cabang',
        'cabang' => $cabang,
    );

    $this->load->view('header',$data);
    $this->load->view('cabang',$data);
    $this->load->view('footer');
}

public function users()
{
    $this->db->order_by('user_id', 'desc');

    $users =  $this->db->query('SELECT a.user_id,a.username,a.level,b.cabang,a.last_login from users a LEFT JOIN cabang b ON a.cabang = b.id_cabang')->result_array();
    $data = array(
        'aktif' => 'users',
        'judul' => 'users',
        'users' => $users,
    );

    $this->load->view('header',$data);
    $this->load->view('users',$data);
    $this->load->view('footer');
}

public function contact()
{
    $this->db->order_by('contact_id', 'desc');
    $contact =  $this->db->get('contact')->result_array();
    $data = array(
        'aktif' => 'blog',
        'judul' => 'BLOG',
        'contact' => $contact,
    );

    $this->load->view('header',$data);
    $this->load->view('contact',$data);
    $this->load->view('footer');
}


public function service()
{
    $cabang = $this->session->userdata('cabang');

    $this->db->order_by('tgl', 'desc');
    $service =  $this->db->get_where('user_service',array('cabang' => $cabang))->result_array();

    $get_cabang = $this->db->get_where('cabang',array('id_cabang' => $cabang))->row_array();
    $data = array(
        'aktif' => 'service',
        'judul' => 'Service Cabang '.$get_cabang['cabang'],
        'service' => $service,
    );

    $this->load->view('header',$data);
    $this->load->view('service',$data);
    $this->load->view('footer');
}

function add($flag=0)
{   
    $filename = '';
    if(isset($_POST) && count($_POST) > 0)     
    {   

        $filename=$_FILES['userfile']['name'];
        $filename = str_replace(' ','-',$filename);
        $filename = strtolower($filename);

        if($_FILES['userfile']['name'] != '') {

                //$config['upload_path'] = "/home/arcigee/public_html/img_blog/"; 
            $config['upload_path'] = "./img_blog/"; 
            $config['file_name'] = $filename;
            $config['overwrite'] = TRUE;
            $config["allowed_types"] = 'jpg|jpeg|png';
            $config["max_size"] = 1920;
            $config["max_width"] = 1920;
            $config["max_height"] = 1280;
            $this->load->library('upload', $config);
            
            if(!$this->upload->do_upload()) {               
                echo $this->upload->display_errors();
            } else {
                echo 'success';                                     
            }  
        }
        
        
        $title = $this->input->post('title');
        $url_slug = str_replace(' ','-',$title);
        $url_slug = str_replace(',','',$url_slug);
        $url_slug = str_replace('@','-',$url_slug);
        $url_slug = str_replace('~','-',$url_slug);
        $url_slug = str_replace('`','-',$url_slug);
        $url_slug = str_replace('$','-',$url_slug);
        $url_slug = str_replace('%','-',$url_slug);
        $url_slug = str_replace('^','-',$url_slug);
        $url_slug = str_replace('*','-',$url_slug);
        $url_slug = str_replace('{','-',$url_slug);
        $url_slug = str_replace('}','-',$url_slug);
        $hasil = strtolower($url_slug);
        
        $params = array(
            'title' => $this->input->post('title'),
            'url_slug' => $hasil,
            'meta_title' => $this->input->post('meta_title'),
            'meta_keyword' => $this->input->post('meta_keyword'),
            'image' => $filename,
            'img_title' => $this->input->post('img_title'),
            'img_alt' => $this->input->post('img_alt'),
            'img_capt' => $this->input->post('img_capt'),
            'isi' => $this->input->post('isi'),
            'meta_desc' => $this->input->post('meta_desc'),
            'flag' => $this->input->post('flag'),
            'publish_date' => date('Y-m-d'),
            'modif_date' => date('Y-m-d H:i:s'),
        );
        
        $this->db->insert('article',$params);
        $article_id= $this->db->insert_id();

            //$article_id = $this->Article_model->add_article($params);
        redirect('admin/blog');
    }
    else
    {          
     $data['flag'] = $flag;  
     $this->load->view('header',null);
     $this->load->view('add_blog',$data);
     $this->load->view('footer');
 }
} 


function add_gallery()
{   
   
     $this->load->view('header',null);
     $this->load->view('add_gallery',null);
     $this->load->view('footer');
 
} 

function insert_gallery()
{
   $filename = '';
    

        $filename=$_FILES['userfile']['name'];
        $filename = str_replace(' ','-',$filename);
        $filename = strtolower($filename);

        if($_FILES['userfile']['name'] != '') {

                //$config['upload_path'] = "/home/arcigee/public_html/img_blog/"; 
            $config['upload_path'] = "./gallery/"; 
            $config['file_name'] = $filename;
            $config['overwrite'] = TRUE;
            $config["allowed_types"] = 'jpg|jpeg|png';
            $config["max_size"] = 1920;
            $config["max_width"] = 1920;
            $config["max_height"] = 1280;
            $this->load->library('upload', $config);
            
            if(!$this->upload->do_upload()) {               
                echo $this->upload->display_errors();
            } else {
                echo 'success';                                     
            }  
        }
        
      
        
        $params = array(
            'image' => $filename,
            'date_publish' => date('Y-m-d'),
        );
        
        $this->db->insert('gallery',$params);
        //$article_id= $this->db->insert_id();

        redirect(base_ulr().'admin/gallery');
}

public function update_about($id)
{
  $filename = '';
    

        $filename=$_FILES['userfile']['name'];
        $filename = str_replace(' ','-',$filename);
        $filename = strtolower($filename);

        if($_FILES['userfile']['name'] != '') {

                //$config['upload_path'] = "/home/arcigee/public_html/img_blog/"; 
            $config['upload_path'] = "./assets/images/"; 
            $config['file_name'] = $filename;
            $config['overwrite'] = TRUE;
            $config["allowed_types"] = 'jpg|jpeg|png';
            $config["max_size"] = 1920;
            $config["max_width"] = 1920;
            $config["max_height"] = 1280;
            $this->load->library('upload', $config);
            
            if(!$this->upload->do_upload()) {               
                echo $this->upload->display_errors();
            } else {
                echo 'success';                                     
            }  
        }
        
      
         if($_FILES['userfile']['name'] != '') {
        $params = array(
            'image' => $filename,
            'perusahaan' => $this->input->post('perusahaan'),
            'keterangan' => $this->input->post('keterangan'),
        );
      }else{
         $params = array(
            'perusahaan' => $this->input->post('perusahaan'),
            'keterangan' => $this->input->post('keterangan'),
        );
      }
        
        $this->db->update('about',$params,$id);
        //$article_id= $this->db->insert_id();

        redirect(base_ulr().'admin/about');
}

function form_cabang($id='')
{
    $data='';
    if($id != '')
    {
         $data['cabang'] = $this->db->get_where('cabang',array('id_cabang'=>$id))->row_array();
         $data['id'] = $id;
    
    }

   $this->load->view('header',null);
   $this->load->view('form_cabang',$data);
   $this->load->view('footer');
}


function edit_about($id='')
{
    $data='';
    if($id != '')
    {
         $data['about'] = $this->db->get_where('about',array('id'=>$id))->row_array();
         $data['id'] = $id;
    
    }

   $this->load->view('header',null);
   $this->load->view('form_about',$data);
   $this->load->view('footer');
}


function form_user($id='')
{
    $data='';
    if($id != '')
    {
         $data['users'] = $this->db->get_where('users',array('user_id'=>$id))->row_array();
         $data['id'] = $id;
    
    }

    $data['cabang'] = $this->db->get('cabang')->result_array();
   $this->load->view('header',null);
   $this->load->view('form_users',$data);
   $this->load->view('footer');
}
function add_users()
{
    $data = $this->input->post();
    $add['username'] = $data['username'];
    $add['password'] = md5(sha1($data['password']));
    $add['level'] = $data['jabatan'];
    $add['cabang'] = $data['cabang'];

    $this->db->insert('users',$add);
    redirect(base_url().'admin/users');
}

function edit_users($id)
{
    $data = $this->input->post();
    $add['username'] = $data['username'];
    if($add['password'] != '')
    {
        $add['password'] = md5(sha1($data['password'])); 
    }
   
    $add['level'] = $data['jabatan'];
    $add['cabang'] = $data['cabang'];

    $this->db->update('users',$add,array('user_id' => $id));
    redirect(base_url().'admin/users');
}

function remove_user($id)
{
     if($this->db->delete('users',array('user_id' => $id)))
     {
        redirect(base_url().'admin/users');
     }
    
}

function add_cabang()
{
    $data = $this->input->post();
    $add['cabang'] = $data['cabang'];
    $add['alamat'] = $data['alamat'];
    $add['tlp'] = $data['tlp'];
    $add['fax'] = $data['fax'];

    $this->db->insert('cabang',$add);
    redirect(base_url().'admin/cabang');
}

function edit_cabang($id)
{

     $data = $this->input->post();
    $add['cabang'] = $data['cabang'];
    $add['alamat'] = $data['alamat'];
    $add['tlp'] = $data['tlp'];
    $add['fax'] = $data['fax'];
    

    $this->db->update('cabang',$add, array('id_cabang' => $id));
    redirect(base_url().'admin/cabang');
}



function remove_cabang($id)
{
    if($this->db->delete('cabang',array('id_cabang' => $id)))
    {
         redirect(base_url().'admin/cabang');
    }
   
}

function remove_gallery($id)
{
    if($this->db->delete('gallery',array('id_gallery' => $id)))
    {
         redirect(base_url().'admin/gallery');
    }
   
}



function edit($id_article)
{   

    $data['article'] = $this->db->get_where('article',array('id_article'=>$id_article))->row_array();
    
    if(isset($data['article']['id_article']))
    {


        if(isset($_POST) && count($_POST) > 0)     
        {   

            $filename=$_FILES['userfile']['name'];
            $filename = str_replace(' ','-',$filename);
            $filename = strtolower($filename);
            
            

            if($_FILES['userfile']['name'] != '') {
                $filename=$_FILES['userfile']['name'];
                $filename = str_replace(' ','-',$filename);
                $filename = strtolower($filename);
                
                    //$config['upload_path'] = "/home/arcigee/public_html/img_blog/"; 
                $config['upload_path'] = "./img_blog/"; 
                $config['file_name'] = $filename;
                $config['overwrite'] = TRUE;
                $config["allowed_types"] = 'jpg|jpeg|png';
                $config["max_size"] = 1920;
                $config["max_width"] = 1920;
                $config["max_height"] = 1280;
                $this->load->library('upload', $config);
                
                if(!$this->upload->do_upload()) {               
                    echo $this->upload->display_errors();
                } else {
                        //echo 'success';                                      
                }  
                
                
            }
            
            $title = $this->input->post('title');
            $url_slug = str_replace(' ','-',$title);
            $url_slug = str_replace(',','',$url_slug);
            $url_slug = str_replace('@','-',$url_slug);
            $url_slug = str_replace('~','-',$url_slug);
            $url_slug = str_replace('`','-',$url_slug);
            $url_slug = str_replace('$','-',$url_slug);
            $url_slug = str_replace('%','-',$url_slug);
            $url_slug = str_replace('^','-',$url_slug);
            $url_slug = str_replace('*','-',$url_slug);
            $url_slug = str_replace('{','-',$url_slug);
            $url_slug = str_replace('}','-',$url_slug);
            $hasil = strtolower($url_slug);
            
            
            if($_FILES['userfile']['name'] != '') {
                $params = array(
                 'title' => $this->input->post('title'),
                 'url_slug' => $hasil,
                 'meta_title' => $this->input->post('meta_title'),
                 'meta_keyword' => $this->input->post('meta_keyword'),
                 'image' => $filename,
                 'img_title' => $this->input->post('img_title'),
                 'img_alt' => $this->input->post('img_alt'),
                 'img_capt' => $this->input->post('img_capt'),
                 'isi' => $this->input->post('isi'),
                 'meta_desc' => $this->input->post('meta_desc'),
                 'flag' => $this->input->post('flag'),
                 'modif_date' => date('Y-m-d H:i:s'),
             );
            }else{
                $params = array(
                 'title' => $this->input->post('title'),
                 'url_slug' => $hasil,
                 'meta_title' => $this->input->post('meta_title'),
                 'meta_keyword' => $this->input->post('meta_keyword'),
                 'img_title' => $this->input->post('img_title'),
                 'img_alt' => $this->input->post('img_alt'),
                 'img_capt' => $this->input->post('img_capt'),
                 'isi' => $this->input->post('isi'),
                 'meta_desc' => $this->input->post('meta_desc'),
                 'flag' => $this->input->post('flag'),
                 'modif_date' => date('Y-m-d H:i:s'),
             );
            }
            
            $this->db->where('id_article',$id_article);
            $this->db->update('article',$params);

                // $this->Article_model->update_article($id_article,$params);            
            redirect('admin/blog');
        }
        else
        {

            $this->load->view('header',$data);
            $this->load->view('edit_blog',$data);
            $this->load->view('footer');
        }
    }
    else
        show_error('The article you are trying to edit does not exist.');
} 


function remove($id_article)
{
    $article = $this->db->get_where('article',array('id_article'=>$id_article))->row_array();

    
    if(isset($article['id_article']))
    {
            //$this->Article_model->delete_article($id_article);
        $this->db->delete('article',array('id_article'=>$id_article));
        redirect('admin/blog');
    }
    else
        show_error('The article you are trying to delete does not exist.');
}

}
