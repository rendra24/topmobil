<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$service = $this->db->get_where("article",array('flag' => 1 ))->result_array();
		$data = array(
			'aktif' => 'blog',
			'judul' => 'BLOG',
			'blog' => $service,
		);

		$this->load->view('Welcome/header',$data);
		$this->load->view('blog',$data);
		$this->load->view('Welcome/footer');

	}

	public function detail($url_slug='')
	{
		$blog = $this->db->get_where("article",array('url_slug' => $url_slug))->row_array();
		$data = array(
			'aktif' => 'blog',
			'judul' => 'BLOG',
			'blog' => $blog,
		);

		$this->load->view('Welcome/header',$data);
		$this->load->view('detail',$data);
		$this->load->view('Welcome/footer');

	}

}
