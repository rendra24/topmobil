<!-- single -->
	<div class="blog">
		<div class="container">
			<div class="agile-blog-grids">
				<div class="col-md-8 agile-blog-grid-left">
					<div class="agile-blog-grid">
						<div class="blog-left-right-top">
									<h4><?php echo $blog['title']; ?></h4>
									<p>Posted By <a href="#">Admin</a> &nbsp;&nbsp; on June 2, 2017 &nbsp;&nbsp; </p>
								</div>
						<div class="agile-blog-grid-left-img">
							<a href="single.html"><img src="<?php echo base_url().'img_blog/'.$blog['image'] ?>" alt="" /></a>
						</div>
						<div class="blog-left-grids">
							
							<div class="blog-left-right">
								
								<div class="blog-left-right-bottom">
									<?php echo $blog['isi']; ?>
								</div>
							</div>
							<div class="clearfix"> </div>
						</div>
						
						
						<!-- <div class="opinion">
							<h3>Leave Your Comment</h3>
							<form action="#" method="post">
								<input type="text" name="Name" placeholder="Name" required="">
								<input type="email" name="Email" placeholder="Email" required="">
								<textarea name="Message" placeholder="Message" required=""></textarea>
								<input type="submit" value="SEND">
							</form>
						</div> -->
					</div>
				</div>
				<div class="col-md-4 agile-blog-grid-right">
					<div class="categories">
						<h3>Categories</h3>
						<ul>
							<li><a href="#">Phasellus sem leo, interdum quis risus</a></li>
							<li><a href="#">Nullam egestas nisi id malesuada aliquet </a></li>
							<li><a href="#"> Donec condimentum purus urna venenatis</a></li>
							<li><a href="#">Ut congue, nisl id tincidunt lobor mollis</a></li>
							<li><a href="#">Cum sociis natoque penatibus et magnis</a></li>
							<li><a href="#">Suspendisse nec magna id ex pretium</a></li>
						</ul>
					</div>
					<div class="categories">
						<h3>Archive</h3>
						<ul class="marked-list offs1">
							<li><a href="#">May 2017 (7)</a></li>
							<li><a href="#">April 2017 (11)</a></li>
							<li><a href="#">March 2017 (12)</a></li>
							<li><a href="#">February 2017 (14)</a> </li>
							<li><a href="#">January 2017 (10)</a></li>    
							<li><a href="#">December 2017 (12)</a></li>
							<li><a href="#">November 2017 (8)</a></li>
							<li><a href="#">October 2017 (7)</a> </li>
							<li><a href="#">September 2017 (8)</a></li>
							<li><a href="#">August 2017 (6)</a></li>                          
						</ul>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- //single -->