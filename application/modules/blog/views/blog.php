<style type="text/css">
	.blog-left-right-bottom p {
  
    margin: 1em 0 1em 0;
}

.blog-left-right-top p {
    margin: 0 0 -1em;
}
</style>
<!-- blog -->
	<div class="blog">
		<div class="container">
			<div class="agile-blog-grids">
				<div class="col-md-8 agile-blog-grid-left">

					<?php foreach ($blog as $row) { ?>

					<div class="agile-blog-grid row">
						<div class="agile-blog-grid-left-img col-md-4">
							<a href="single.html"><img src="<?php echo base_url().'img_blog/'.$row['image'] ?>" alt="" /></a>
						</div>
						<div class="blog-left-grids col-md-8">
							
							<div class="blog-left-right ">
								<div class="blog-left-right-top">
									<h4><a href="<?php echo base_url().'blog/detail/'.$row['url_slug']; ?>"><?php echo $row['title']; ?></a></h4>
									<p>Posted By <a href="#">Admin</a> &nbsp;&nbsp; on June 2, 2017 &nbsp;&nbsp; </p>
								</div>
								<div class="blog-left-right-bottom">
									<p><?php echo substr($row['isi'], 0,90); ?></p>
									<a href="<?php echo base_url().'blog/detail/'.$row['url_slug']; ?>">More</a>
								</div>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
					<?php } ?>
					

					

					<nav>
						<ul class="pagination">
							<li>
								<a href="#" aria-label="Previous">
									<span aria-hidden="true">«</span>
								</a>
							</li>
							<li><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li>
								<a href="#" aria-label="Next">
									<span aria-hidden="true">»</span>
								</a>
							</li>
						</ul>
					</nav>
				</div>
				<div class="col-md-4 agile-blog-grid-right">
					
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- //blog -->